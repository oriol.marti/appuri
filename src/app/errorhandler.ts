/* eslint-disable max-len */
import { ErrorHandler, Injectable, ViewChild } from '@angular/core';
import { Router, NavigationError, ActivationStart, RouterOutlet } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ErrorAlerter implements ErrorHandler {
  @ViewChild(RouterOutlet) outlet: RouterOutlet;
  contador = 0;

  constructor( private router: Router) {
    this.router.events.subscribe(e => {
      if (e instanceof ActivationStart && e.snapshot.outlet === 'login') { this.outlet.deactivate(); }
    });
  }

  handleError(error) {
    /* console.error(error); */
    localStorage.setItem('EHC', JSON.stringify(this.contador));
    this.contador++;
    if(error instanceof NavigationError){
      this.router.events.subscribe(e => {
        if (e instanceof ActivationStart && e.snapshot.outlet === 'login') { this.outlet.deactivate(); }
      });
      //this.router.parseUrl('/login');
      };

    if(error.message === 'Unexpected token \'\', "ée" is not valid JSON'){
      if(JSON.parse(localStorage.getItem('EHC')) < 0){
        window.location.reload();
      }else{
        localStorage.setItem('EHC', '0');
        this.router.events.subscribe(e => {
          if (e instanceof ActivationStart && e.snapshot.outlet === 'login') { this.outlet.deactivate(); }
        });
        console.log(error.message);
        //this.router.navigate(['login']);
      }
      //window.location.reload();
      //this.router.navigate(['login']);
    }
  }
}
