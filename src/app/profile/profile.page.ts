/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/dot-notation */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { ModalController, Platform, ToastController, ViewWillEnter } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import * as firebase from 'firebase/compat';
import { empty } from 'rxjs';
import { EditProfileComponent } from '../components/edit-profile/edit-profile.component';
import { LegalComponent } from '../components/legal/legal.component';
import { PrivacyComponent } from '../components/privacy/privacy.component';
import { TermsyconditionsComponent } from '../components/termsyconditions/termsyconditions.component';
import { User } from '../interfaces/users';
import { AuthStorageService } from '../services/auth-storage.service';
import { IonicAuthService } from '../services/ionic-auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit, ViewWillEnter {
  userDetail: User;
  userRole = 'user';

  edit: string;
  passch: string;
  exit: string;
  priv: string;
  legal: string;
  termcond: string;
  public language: string;
  private subscription: any;


  constructor(
    private router: Router,
    private ionicAuthService: IonicAuthService,
    private ionserviceStorageAuth: AuthStorageService,
    private modalCntrl: ModalController,
    private globalization: Globalization,
    private _translate: TranslateService,
    private platform: Platform,
    private toastController: ToastController,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    if (localStorage.getItem('token') === 'null') {
      //this.router.navigate(['login']);
      return 1;
    } else {
      const localdata = JSON.parse(atob(localStorage.getItem('token')));
      this.userDetail = localdata;
      this.userRole = this.userDetail.role;
    };
  }

  async profileeditmodal(id) {
    const modal = await this.modalCntrl.create({
      component: EditProfileComponent,
      breakpoints: [0, 1],
      componentProps: { id },
    });
    await modal.present();
  }
  /*  this.ionserviceStorageAuth.editUserDocument(this.userDetail.email, this.userDetail); */

  signOut() {
    this.ionicAuthService.signoutUser().then(()=>{this.router.navigate(['login']);});
  }

  /**
   * Executes every moment u enter the view
   *
   * @uri
   */
  async ionViewWillEnter() {
    //this.updateUserData();
            if(localStorage.getItem('lenguage')){
          this.initTranslateDefault(localStorage.getItem('lenguage'));
        }else{
          this.getDeviceLanguage();
        }
  }

  /**
   * Update the user in the localstorage based on your current user auth call
   *
   * @uri
   */
  updateUserData() {
    let currentUser: User;
    this.ionicAuthService.currentUser().then(async (user) => {
      const emailUser = user.email;
      this.ionserviceStorageAuth.getUser(user.email).subscribe(
        // eslint-disable-next-line @typescript-eslint/no-shadow
        user => {
          currentUser = {
            email: emailUser,
            displayName: user['displayName'],
            role: user['role'],
            brand: user['brand'],
            linkAccount: user['linkAccount'],
            assignedProjects: user['assignedProjects'],
            img: user['img']
          };
          localStorage.setItem('token', btoa(JSON.stringify(currentUser)));
        }
      );
    });
    //this.ionserviceStorageAuth.getUser();
    const localdata = JSON.parse(atob(localStorage.getItem('token')));
    this.userDetail = localdata;
    this.userRole = this.userDetail.role;
  }

  doRefresh(event) {
    // console.log('refresh');
    setTimeout(() => {
      this.updateUserData();
      event.target.complete();
      if (event.cancelable) { event.preventDefault(); }
    }, 4000);
  }

  ionViewDidEnter() {
        /* LENGUAGE */
        if(localStorage.getItem('lenguage')){
          this.initTranslateDefault(localStorage.getItem('lenguage'));
        }else{
          this.getDeviceLanguage();
        }
  }

  _initialiseTranslation(): void {
    this._translate.get('PROFILE.edit').subscribe((res: string) => {
      this.edit = res;
    });
    this._translate.get('PROFILE.passch').subscribe((res: string) => {
      this.passch = res;
    });
    this._translate.get('PROFILE.exit').subscribe((res: string) => {
      this.exit = res;
    });
    this._translate.get('MENU.priv').subscribe((res: string) => {
      this.priv = res;
    });
    this._translate.get('MENU.termcond').subscribe((res: string) => {
      this.termcond = res;
    });
    this._translate.get('MENU.legal').subscribe((res: string) => {
      this.legal = res;
    });
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initTranslate(language: string) {
    if (language) {
      this.language = this._translate.getBrowserLang();
    }
    else {
      // Set your language here
      this.language = 'en';
    }
    this._translateLanguage();;
  }

  initTranslateDefault(language: string) {
    if (language) {
      this.language = language;
    }
    this._translateLanguage();;
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === 'object') {
      this._initTranslate(navigator.language);
    }
    else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value);
        })
        .catch(e => {
          // console.log(e);
        });
    }
  }

  resetPassword(){
    this.ionicAuthService.rePassword(this.userDetail.email);
    this.displayToast();

  }

  displayToast() {
    try {
      this.toastController.dismiss().then(() => {
      }).catch(() => {
      }).finally(() => {
        // console.log('Closed');
      });
    } catch (e) { }

    this.toastController.create({
      message: 'Correo enviado',
      position: 'middle',
      duration: 2000,
      animated: true,
      cssClass: 'toast-custom-class'
    }).then((toast) => {
      toast.present();
    });
  }

  async openPrivacyModal(){
    const modal = await this.modalController.create({
      component: PrivacyComponent,
      componentProps: {},
    });
    await modal.present();

  }

  async openLegalModal(){
    const modal = await this.modalController.create({
      component: LegalComponent,
      componentProps: {},
    });
    await modal.present();

  }

  async openTermsandConditionsModal(){
    const modal = await this.modalController.create({
      component: TermsyconditionsComponent,
      componentProps: {},
    });
    await modal.present();

  }
}
