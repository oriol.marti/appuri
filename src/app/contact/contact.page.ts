/* eslint-disable no-underscore-dangle */
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivationStart, Router, RouterOutlet } from '@angular/router';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { AlertController, ModalController, Platform, ToastController, ViewWillEnter } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../interfaces/users';
import { IonicAuthService } from '../services/ionic-auth.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit, ViewWillEnter {
  @ViewChild(RouterOutlet) outlet: RouterOutlet;
  userDetail: User;
  userRole = 'user';

  serv= 'Services';
  title1= 'Specialists in stand design';
  title2= 'Customized solutions.';
  text1= 'Company leader in desing, construction and assembly of stands, ephemeral spaces, events and solutions in retail and design.';
  text2= 'Let us advise you for greater participation in the fairs.';
  contact= 'Contact us';
  public language: string;
  private subscription: any;


  constructor(private router: Router,
    private ionicAuthService: IonicAuthService,
    public modalController: ModalController,
    public toastController: ToastController,
    private globalization: Globalization,
    private _translate: TranslateService,
    private platform: Platform,
    private alertController: AlertController
  ) {
    this.router.events.subscribe(e => {
      if (e instanceof ActivationStart && e.snapshot.outlet === 'login') { this.outlet.deactivate(); }
    });
    if(!localStorage.getItem('lenguage')){
      this.getDeviceLanguage();
    }
    if (localStorage.getItem('token') === 'null' || !localStorage.getItem('token')) {
      this.ionicAuthService.signoutUser().then(()=>{this.router.navigate(['login']);});
    }
  }

  ngOnInit() {
    if (localStorage.getItem('token') === 'null') {
      this.ionicAuthService.signoutUser().then(()=>{this.router.navigate(['login']);});
    } else {
      const localdata = JSON.parse(atob(localStorage.getItem('token')));
      this.userDetail = localdata;
      this.userRole = this.userDetail.role;
    };
  }

  sendEmail() {
    this.router.navigate(['/email']);
  }

  ionViewWillEnter() {
    const localdata = JSON.parse(atob(localStorage.getItem('token')));
    this.userDetail = localdata;
    this.userRole = this.userDetail.role;
    if(localStorage.getItem('lenguage')){
      this.initTranslateDefault(localStorage.getItem('lenguage'));
    }else{
      this.getDeviceLanguage();
    }
  }

  _initialiseTranslation(): void {
    this._translate.get('HOME.serv').subscribe((res: string) => {
      this.serv = res;
    });
    this._translate.get('HOME.title1').subscribe((res: string) => {
      this.title1 = res;
    });
    this._translate.get('HOME.title2').subscribe((res: string) => {
      this.title2 = res;
    });
    this._translate.get('HOME.text1').subscribe((res: string) => {
      this.text1 = res;
    });
    this._translate.get('HOME.text2').subscribe((res: string) => {
      this.text2 = res;
    });
    this._translate.get('HOME.contact').subscribe((res: string) => {
      this.contact = res;
    });
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initTranslate(language: string) {
    if (language) {
      this.language = this._translate.getBrowserLang();
    }
    else {
      // Set your language here
      this.language = 'en';
    }
    this._translateLanguage();;
  }

  initTranslateDefault(language: string) {
    if (language) {
      this.language = language;
    }
    this._translateLanguage();;
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === 'object') {
      this._initTranslate(this._translate.getBrowserLang());
    }
    else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value);
        })
        .catch(e => {
          // console.log(e);
        });
    }
  }

}
