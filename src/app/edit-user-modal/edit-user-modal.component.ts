import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { User } from '../interfaces/users';
import { AuthStorageService } from '../services/auth-storage.service';
import { IonicAuthService } from '../services/ionic-auth.service';

@Component({
  selector: 'app-edit-user-modal',
  templateUrl: './edit-user-modal.component.html',
  styleUrls: ['./edit-user-modal.component.scss'],
})
export class EditUserModalComponent implements OnInit {
  userDetail: User;
  linkAccountsForm: FormGroup;
  projectsForm: FormGroup;
  successMsg = '';
  errorMsg = '';
  // eslint-disable-next-line @typescript-eslint/naming-convention
  error_msg = {
    projects: [
      {
        type: 'required',
        message: 'Inserta un proyecto'
      },
    ],
    linkAccounts: [
      {
        type: 'required',
        message: 'Inserta un proyecto'
      },
    ]
  };
  constructor(
    private fb: FormBuilder,
    private ionicAuthService: IonicAuthService,
    private router: Router,
    public modalController: ModalController,
    private stgUserProject: AuthStorageService,
  ) { }

  ngOnInit() {
    const localdata = JSON.parse(atob(localStorage.getItem('token')));
    this.userDetail = localdata;
    this.projectsForm = this.fb.group({
      projects: new FormControl('', Validators.compose([Validators.required]))
    });
    this.linkAccountsForm = this.fb.group({
      linkAccounts: new FormControl('', Validators.compose([Validators.required]))
    });
  }

  async canDismiss() {
    this.modalController.dismiss();
  }

  setProjectsData(formvalues) {
    const projects = this.userDetail.assignedProjects;
    projects.push(formvalues.projects);
    this.stgUserProject.updateProjectsData(this.userDetail.email, projects);
  }
  setlinkAccount(linkaccount) {
    this.stgUserProject.updateLinkAccount(this.userDetail.email, linkaccount);
  }
}
