/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-underscore-dangle */
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController, Platform, ToastController } from '@ionic/angular';
import { User } from 'src/app/interfaces/users';
import { AuthStorageService } from 'src/app/services/auth-storage.service';
import { EmailAuthProvider, getAuth, updateEmail } from 'firebase/auth';
import { Router } from '@angular/router';
import { IonicAuthService } from 'src/app/services/ionic-auth.service';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss'],
})
export class EditProfileComponent implements OnInit {
  @Input() id: string;
  user: User;
  userForm: FormGroup;
  successMsg = '';
  errorMsg = '';
  userDetail: User;
  password: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  error_msg = {
    displayName: [
      {
        type: 'required',
        message_es: 'Introduce un nombre de usuario.',
        message_en: 'Provide a username'
      },
      {
        type: 'minlength',
        message_es: 'Nombre no valido.',
        message_en: 'Naemo is not valid'
      }
    ],
    email: [
      {
        type: 'required',
        message_es: 'Introduce un email valido.',
        message_en: 'Provide a email'
      },
      {
        type: 'pattern',
        message_es: 'Email no es valido.',
        message_en: 'Email is not valid'
      }
    ],
    password: [
      {
      type:'required',
      message:'introduce tu contraseña',
      message_en: 'Provide your password'
      }
    ]
  };
  param = {value: 'world'};

  title: string;
  username: string;
  save: string;

  public language: string;
  private subscription: any;

  constructor(
    private modalCntrl: ModalController,
    private ionserviceStorageAuth: AuthStorageService,
    private fb: FormBuilder,
    private toastController: ToastController,
    private router: Router,
    private ionAuth: IonicAuthService,
    private globalization: Globalization,
    private _translate: TranslateService,
    private platform: Platform,
    private angularFireAuth: AngularFireAuth,
  ) {
      this.user = JSON.parse(atob(localStorage.getItem('token')));
    }

  ngOnInit() {
    if(localStorage.getItem('token') === 'null' || !localStorage.getItem('token')){
      //this.router.navigateByUrl('login');
      return 1;
    }else{
    const localdata = JSON.parse(atob(localStorage.getItem('token')));
    this.userDetail = localdata;
    };
    this.userForm = this.fb.group({
      displayName: new FormControl(this.userDetail.displayName, Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      email: new FormControl(this.userDetail.email, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl(this.password, Validators.compose([
        Validators.required
      ])),
    });
  }
  async canDismiss() {
    this.router.navigateByUrl('tabs/tab3').then(() => {
      window.location.reload();
    });

    this.modalCntrl.dismiss();
  }

  editprofile(userForm){
    const userData: User = {
      email: userForm.email,
      displayName: userForm.displayName,
      role: this.user.role,
      brand: this.user.brand,
      linkAccount: this.user.linkAccount,
      assignedProjects: this.user.assignedProjects,
      img: this.user.img,
    };
    this.editAuth(userData.email,userForm.password ).then(()=>{
      this.ionserviceStorageAuth.editUserDocument(this.id, userData);
      this.ionAuth.setLocalData();
    });

  }

  async editAuth(newemail: string, password: string){
    try{
      const user = this.angularFireAuth.currentUser;
      (await user).reauthenticateWithCredential(EmailAuthProvider.credential(this.userDetail.email, password));
          //.reauthenticateWithCredential(credentials);

    updateEmail(await user, newemail).then(() => {
      this.toast('bottom', 'Información del usuario actualizada.');
      this.canDismiss();
    }).catch((error) => {
      // console.log(error);
      this.toast('bottom', 'Error, no se ha podido actualizar la información del usuario.');
    });
    }catch (err) {
      console.error('err', { err });
    }


  }

  async toast(position: 'top' | 'middle' | 'bottom', massage: string) {
    const toast = await this.toastController.create({
      // eslint-disable-next-line max-len
      message: massage,
      duration: 1500,
      // eslint-disable-next-line object-shorthand
      position: position,
      cssClass: 'custom_toast',
    });

    await toast.present();
  }
  ionViewWillEnter() {
    if(localStorage.getItem('lenguage')){
      this.initTranslateDefault(localStorage.getItem('lenguage'));
    }else{
      this.getDeviceLanguage();
    }
  }

  _initialiseTranslation(): void {
    this._translate.get('EDIT_PROFILE.title').subscribe((res: string) => {
      this.title = res;
    });
    this._translate.get('EDIT_PROFILE.username').subscribe((res: string) => {
      this.username = res;
    });
    this._translate.get('EDIT_PROFILE.save').subscribe((res: string) => {
      this.save = res;
    });
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initTranslate(language: string) {
    if (language) {
      this.language = this._translate.getBrowserLang();
    }
    else {
      // Set your language here
      this.language = 'en';
    }
    this._translateLanguage();;
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === 'object') {
      this._initTranslate(navigator.language);
    }
    else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value);
        })
        .catch(e => {
          // console.log(e);
        });
    }
  }

  initTranslateDefault(language: string) {
    if (language) {
      this.language = language;
    }
    this._translateLanguage();;
  }
}
