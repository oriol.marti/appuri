/* eslint-disable no-underscore-dangle */
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { CheckboxCustomEvent, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-termsyconditions',
  templateUrl: './termsyconditions.component.html',
  styleUrls: ['./termsyconditions.component.scss'],
})
export class TermsyconditionsComponent implements OnInit {
  arrayfav: boolean | void;
  canDismiss: any;

  public language: string;

  title: string;
  subtitle: string;
  question: string;
  termsandcondtionsparm1: string;
  termsandcondtionsparm2: string;
  termsandcondtionsparm3: string;
  termsandcondtionsparm4: string;
  termsandcondtionsparm5: string;
  termsandcondtionsparm6: string;
  termsandcondtionsparm7: string;
  termsandcondtionsparm7a: string;
  termsandcondtionsparm7b: string;
  termsandcondtionsparm7c: string;
  termsandcondtionsparm8: string;
  termsandcondtionsparm9: string;
  termsandcondtionsparm10:  string;
  termsandcondtionsparm11: string;
  termsandcondtionsparm12: string;
  button: string;

  presentingElement = null;
  constructor(
    public modalController: ModalController,
    private globalization: Globalization,
    private _translate: TranslateService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {this.canDismiss = false; }

  async ngOnInit() {
    if(localStorage.getItem('CPTC')){this.canDismiss = localStorage.getItem('CPTC');}
  }

  ionViewDidEnter() {
    if(localStorage.getItem('lenguage')){
      this.initTranslateDefault(localStorage.getItem('lenguage'));
    }else{
      this.getDeviceLanguage();
    }
  }


  checked(booleanCheck) {
    localStorage.setItem('CPTC', booleanCheck);
  }

  onTermsChanged(event: Event) {
    const ev = event as CheckboxCustomEvent;
    this.canDismiss = ev.detail.checked;
    if(!this.canDismiss){
      this.checked('false');
    }else{
      this.checked('true');
    }
  }

  _initialiseTranslation(): void {
    this._translate.get('terms&conditions.title').subscribe((res: string) => {
      this.title = res;
    });
    this._translate.get('terms&conditions.subtitle').subscribe((res: string) => {
      this.subtitle = res;
    });
    this._translate.get('terms&conditions.question').subscribe((res: string) => {
      this.question = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm1').subscribe((res: string) => {
      this.termsandcondtionsparm1 = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm2').subscribe((res: string) => {
      this.termsandcondtionsparm2 = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm3').subscribe((res: string) => {
      this.termsandcondtionsparm3 = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm4').subscribe((res: string) => {
      this.termsandcondtionsparm4 = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm5').subscribe((res: string) => {
      this.termsandcondtionsparm5 = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm6').subscribe((res: string) => {
      this.termsandcondtionsparm6 = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm7').subscribe((res: string) => {
      this.termsandcondtionsparm7 = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm7a').subscribe((res: string) => {
      this.termsandcondtionsparm7a = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm7b').subscribe((res: string) => {
      this.termsandcondtionsparm7b = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm7c').subscribe((res: string) => {
      this.termsandcondtionsparm7c = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm8').subscribe((res: string) => {
      this.termsandcondtionsparm8 = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm9').subscribe((res: string) => {
      this.termsandcondtionsparm9 = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm10').subscribe((res: string) => {
      this.termsandcondtionsparm10 = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm11').subscribe((res: string) => {
      this.termsandcondtionsparm11 = res;
    });
    this._translate.get('terms&conditions.termsandcondtionsparm12').subscribe((res: string) => {
      this.termsandcondtionsparm12 = res;
    });
    this._translate.get('terms&conditions.button').subscribe((res: string) => {
      this.button = res;
    });
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initTranslate(language: string) {
    if (language) {
      this.language = this._translate.getBrowserLang();
    }
    else {
      // Set your language here
      this.language = 'en';
    }
    this._translateLanguage();;
  }

  initTranslateDefault(language: string) {
    if (language) {
      this.language = language;
    }
    this._translateLanguage();;
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === 'object') {
      this._initTranslate(navigator.language);
    }
    else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value);
        })
        .catch(e => {
          // console.log(e);
        });
    }
  }
}
