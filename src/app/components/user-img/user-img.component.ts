/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { FILE } from '../../interfaces/file.class';
import {AngularFireStorage,AngularFireUploadTask} from '@angular/fire/compat/storage';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { User } from 'src/app/interfaces/users';
import { doc, getDoc, getDocFromCache, getDocFromServer, getFirestore } from 'firebase/firestore';
import { stringify } from 'querystring';
import { AuthStorageService } from 'src/app/services/auth-storage.service';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-user-img',
  templateUrl: './user-img.component.html',
  styleUrls: ['./user-img.component.scss'],
})
export class UserImgComponent {
  userDetail: User;
  editForm: FormGroup;
  id: string;

  ngFireUploadTask: AngularFireUploadTask;

  progressNum: Observable<number>;

  progressSnapshot: Observable<any>;

  fileUploadedPath: Observable<string>;

  files: Observable<FILE[]>;
  photo= JSON.parse(localStorage.getItem('profilephoto'));

  FileName: string;
  FileSize: number;

  isImgUploading: boolean;
  isImgUploaded: boolean;

  progress: string;
  img_size: string;
  trans: string;
  cancel: string;
  pause: string;
  restart: string;
  profile_updated: string;
  current_img: string;

  public language: string;
  private subscription: any;

  private ngFirestoreCollection: AngularFirestoreCollection<FILE>;

  constructor( private activatedRoute: ActivatedRoute,
        private router: Router,
        public formBuilder: FormBuilder,
        private angularFirestore: AngularFirestore,
        private angularFireStorage: AngularFireStorage,
        private ionAuthStorage: AuthStorageService,
        private globalization: Globalization,
        private _translate: TranslateService,
        private platform: Platform,
        )
        {
          const localdata = JSON.parse(atob(localStorage.getItem('token')));
          this.userDetail = localdata;
          // eslint-disable-next-line max-len

          this.isImgUploading = false;
          this.isImgUploaded = false;
          this.ngFirestoreCollection = angularFirestore.collection<FILE>('userProfilePhotos');
          this.files = this.ngFirestoreCollection.valueChanges();

  }

    fileStorage(image: FILE) {
      const ImgId = this.angularFirestore.createId();

      this.ngFirestoreCollection.doc(this.userDetail.email).set(image).then(data => {
        // console.log(data);
      }).catch(error => {
        // console.log(error);
      });
  }

  fileUpload(event: FileList) {

    const file = event.item(0);

    if (file.type.split('/')[0] !== 'image') {
      // console.log('File type is not supported!');
      return;
    }

    this.isImgUploading = true;
    this.isImgUploaded = false;

    this.FileName = file.name;

    const fileStoragePath = `filesStorage/${new Date().getTime()}_${file.name}`;

    const imageRef = this.angularFireStorage.ref(fileStoragePath);

    this.ngFireUploadTask = this.angularFireStorage.upload(fileStoragePath, file);

    this.progressNum = this.ngFireUploadTask.percentageChanges();
    this.progressSnapshot = this.ngFireUploadTask.snapshotChanges().pipe(

      finalize(() => {
        this.fileUploadedPath = imageRef.getDownloadURL();

        this.fileUploadedPath.subscribe(resp=>{
          this.ionAuthStorage.updateImg(this.userDetail.email, resp);
          this.userDetail = {
            email: this.userDetail.email,

            displayName: this.userDetail.displayName,

            role: this.userDetail.role,

            brand: this.userDetail.brand,

            linkAccount: this.userDetail.linkAccount,

            assignedProjects: this.userDetail.assignedProjects,

             img: resp};

            localStorage.setItem('token', btoa(JSON.stringify(this.userDetail)));
          /* this.fileStorage({
            name: file.name,
            filepath: resp,
            idItem: this.userDetail.email,
            size: this.FileSize
          }); */
          this.isImgUploading = false;
          this.isImgUploaded = true;
        },error => {
          // console.log(error);
        });
      }),
      tap(snap => {
          this.FileSize = snap.totalBytes;
      })
    );
}

ionViewDidEnter() {
  if(localStorage.getItem('lenguage')){
    this.initTranslateDefault(localStorage.getItem('lenguage'));
  }else{
    this.getDeviceLanguage();
  }
}

_initialiseTranslation(): void {
  this._translate.get('USER_IMG.progress').subscribe((res: string) => {
    this.progress = res;
  });
  this._translate.get('USER_IMG.img_size').subscribe((res: string) => {
    this.img_size = res;
  });
  this._translate.get('USER_IMG.trans').subscribe((res: string) => {
    this.trans = res;
  });
  this._translate.get('USER_IMG.cancel').subscribe((res: string) => {
    this.cancel = res;
  });
  this._translate.get('USER_IMG.pause').subscribe((res: string) => {
    this.pause = res;
  });
  this._translate.get('USER_IMG.restart').subscribe((res: string) => {
    this.restart = res;
  });
  this._translate.get('USER_IMG.profile_updated').subscribe((res: string) => {
    this.profile_updated = res;
  });
  this._translate.get('USER_IMG.current_img').subscribe((res: string) => {
    this.current_img = res;
  });
}

_translateLanguage(): void {
  this._translate.use(this.language);
  this._initialiseTranslation();
}

_initTranslate(language: string) {
  // Set the default language for translation strings, and the current language.
  this._translate.setDefaultLang('es-ES');
  if (language) {
    this.language = language;
  }
  else {
    // Set your language here
    this.language = 'es-ES';
  }
  this._translateLanguage();;
}

getDeviceLanguage() {
  if (window.Intl && typeof window.Intl === 'object') {
    this._initTranslate(navigator.language);
  }
  else {
    this.globalization.getPreferredLanguage()
      .then(res => {
        this._initTranslate(res.value);
      })
      .catch(e => {
        // console.log(e);
      });
  }
}

initTranslateDefault(language: string) {
  if (language) {
    this.language = language;
  }
  this._translateLanguage();;
}

}
