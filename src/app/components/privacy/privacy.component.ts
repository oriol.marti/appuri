/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-underscore-dangle */
import { Component, OnInit } from '@angular/core';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { CheckboxCustomEvent, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss'],
})
export class PrivacyComponent implements OnInit {
  canDismiss: any;

  public language: string;
  title: string;
  subtitle: string;
  question: string;
  fecha_edicion: string;
    para0: string;
    title1: string;
      para1_1: string;
    title2: string;
      para2_1: string;
      para2_2: string;
      para2_3: string;
    title3: string;
      para3_1: string;
      para3_p1: string;
      para3_p2: string;
      para3_2: string;
    title4: string;
      para4_1: string;
      para4_2: string;
    title5: string;
      para5_1: string;
      para5_2: string;
    title6: string;
      para6_1: string;
      para6_2: string;
      para6_3: string;
    title7: string;
      para7_1: string;
      para7_2: string;
      para7_3: string;
    title8: string;
      para8_1: string;
      para8_2: string;
    title9: string;
      para9_1: string;
      para9_2: string;
      para9_3: string;

  constructor(
    public modalController: ModalController,
    private globalization: Globalization,
    private _translate: TranslateService,
  ) {this.canDismiss = false;}

  async ngOnInit() {
    if(localStorage.getItem('privacy')){this.canDismiss = localStorage.getItem('privacy');}
  }

  ionViewDidEnter() {
    if(localStorage.getItem('lenguage')){
      this.initTranslateDefault(localStorage.getItem('lenguage'));
    }else{
      this.getDeviceLanguage();
    }
  }

  checked(booleanCheck) {
    localStorage.setItem('privacy', booleanCheck);
  }

  onTermsChanged(event: Event) {
    const ev = event as CheckboxCustomEvent;
    this.canDismiss = ev.detail.checked;
    if(!this.canDismiss){
      this.checked('false');
    }else{
      this.checked('true');
    }
  }

  _initialiseTranslation(): void {
    this._translate.get('PRIVACIDAD.title').subscribe((res: string) => {
      this.title = res;
    });
    this._translate.get('PRIVACIDAD.subtitle').subscribe((res: string) => {
      this.subtitle = res;
    });
    this._translate.get('PRIVACIDAD.question').subscribe((res: string) => {
      this.question = res;
    });
    this._translate.get('PRIVACIDAD.fecha edicion').subscribe((res: string) => {
      this.fecha_edicion = res;
    });
    this._translate.get('PRIVACIDAD.para0').subscribe((res: string) => {
      this.para0 = res;
    });
    this._translate.get('PRIVACIDAD.title1').subscribe((res: string) => {
      this.title1 = res;
    });
    this._translate.get('PRIVACIDAD.para1-1').subscribe((res: string) => {
      this.para1_1 = res;
    });
    this._translate.get('PRIVACIDAD.title2').subscribe((res: string) => {
      this.title2 = res;
    });
    this._translate.get('PRIVACIDAD.para2-1').subscribe((res: string) => {
      this.para2_1 = res;
    });
    this._translate.get('PRIVACIDAD.para2-2').subscribe((res: string) => {
      this.para2_2 = res;
    });
    this._translate.get('PRIVACIDAD.para2-3').subscribe((res: string) => {
      this.para2_3 = res;
    });
    this._translate.get('PRIVACIDAD.title3').subscribe((res: string) => {
      this.title3 = res;
    });
    this._translate.get('PRIVACIDAD.para3-1').subscribe((res: string) => {
      this.para3_1 = res;
    });
    this._translate.get('PRIVACIDAD.para3-p1').subscribe((res: string) => {
      this.para3_p1 = res;
    });
    this._translate.get('PRIVACIDAD.para3-p2').subscribe((res: string) => {
      this.para3_p2 = res;
    });
    this._translate.get('PRIVACIDAD.para3-2').subscribe((res: string) => {
      this.para3_2 = res;
    });
    this._translate.get('PRIVACIDAD.title4').subscribe((res: string) => {
      this.title4 = res;
    });
    this._translate.get('PRIVACIDAD.para4-1').subscribe((res: string) => {
      this.para4_1 = res;
    });
    this._translate.get('PRIVACIDAD.para4-2').subscribe((res: string) => {
      this.para4_2 = res;
    });
    this._translate.get('PRIVACIDAD.title5').subscribe((res: string) => {
      this.title5 = res;
    });
    this._translate.get('PRIVACIDAD.para5-1').subscribe((res: string) => {
      this.para5_1 = res;
    });
    this._translate.get('PRIVACIDAD.para5-2').subscribe((res: string) => {
      this.para5_2 = res;
    });
    this._translate.get('PRIVACIDAD.title6').subscribe((res: string) => {
      this.title6 = res;
    });
    this._translate.get('PRIVACIDAD.para6-1').subscribe((res: string) => {
      this.para6_1 = res;
    });
    this._translate.get('PRIVACIDAD.para6-2').subscribe((res: string) => {
      this.para6_2 = res;
    });
    this._translate.get('PRIVACIDAD.para6-3').subscribe((res: string) => {
      this.para6_3 = res;
    });
    this._translate.get('PRIVACIDAD.title7').subscribe((res: string) => {
      this.title7 = res;
    });
    this._translate.get('PRIVACIDAD.para7-1').subscribe((res: string) => {
      this.para7_1 = res;
    });
    this._translate.get('PRIVACIDAD.para7-2').subscribe((res: string) => {
      this.para7_2 = res;
    });
    this._translate.get('PRIVACIDAD.para7-3').subscribe((res: string) => {
      this.para7_3 = res;
    });
    this._translate.get('PRIVACIDAD.title8').subscribe((res: string) => {
      this.title8 = res;
    });
    this._translate.get('PRIVACIDAD.para8-1').subscribe((res: string) => {
      this.para8_1 = res;
    });
    this._translate.get('PRIVACIDAD.para8-2').subscribe((res: string) => {
      this.para8_2 = res;
    });
    this._translate.get('PRIVACIDAD.title9').subscribe((res: string) => {
      this.title9 = res;
    });
    this._translate.get('PRIVACIDAD.para9-1').subscribe((res: string) => {
      this.para9_1 = res;
    });
    this._translate.get('PRIVACIDAD.para9-2').subscribe((res: string) => {
      this.para9_2 = res;
    });
    this._translate.get('PRIVACIDAD.para9-3').subscribe((res: string) => {
      this.para9_3 = res;
    });
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initTranslate(language: string) {
    if (language) {
      this.language = this._translate.getBrowserLang();
    }
    else {
      // Set your language here
      this.language = 'en';
    }
    this._translateLanguage();;
  }

  initTranslateDefault(language: string) {
    if (language) {
      this.language = language;
    }
    this._translateLanguage();;
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === 'object') {
      this._initTranslate(navigator.language);
    }
    else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value);
        })
        .catch(e => {
          // console.log(e);
        });
    }
  }

}
