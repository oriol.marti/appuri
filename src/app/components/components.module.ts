import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MenuComponent } from './menu/menu.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileSizePipe } from '../pipes/file-size.pipe';
import { UserImgComponent } from './user-img/user-img.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsyconditionsComponent } from './termsyconditions/termsyconditions.component';
import { LegalComponent } from './legal/legal.component';

@NgModule({
  declarations: [
      MenuComponent,
      EditProfileComponent,
      UserImgComponent,
      FileSizePipe,
      PrivacyComponent,
      TermsyconditionsComponent,
      LegalComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    MenuComponent,
    EditProfileComponent,
    UserImgComponent,
    PrivacyComponent,
    TermsyconditionsComponent,
    LegalComponent
  ]
})
export class ComponentsModule { }
