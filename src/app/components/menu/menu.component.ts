/* eslint-disable no-underscore-dangle */
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivationStart, Router, RouterOutlet } from '@angular/router';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { ModalController, ToastController, AnimationController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../../interfaces/users';
import { IonicAuthService } from '../../services/ionic-auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  @ViewChild(RouterOutlet) outlet: RouterOutlet;
  public language: string;
  userDetail: User;
  userRole = 'user';
  flagImg = './assets/images/uk_flag.png';

  constructor(

    public modalController: ModalController,
    public toastController: ToastController,
    private router: Router,
    private ionicAuthService: IonicAuthService,
    private globalization: Globalization,
    private _translate: TranslateService,
    private animationCtrl: AnimationController,

    ) {
      this.router.events.subscribe(e => {
        if (e instanceof ActivationStart && e.snapshot.outlet === 'login') { this.outlet.deactivate(); }
      });

    if (localStorage.getItem('token') === 'null' || localStorage.getItem('token') == null) {
      // console.log('entro');
        this.router.navigate(['login']);
    } else {
      /* // console.log('storage'+localStorage.getItem('token')); */
      const localdata = JSON.parse(atob(localStorage.getItem('token')));
      this.userDetail = localdata;
      this.userRole = this.userDetail.role;
    }; }


  ngOnInit() {
    const localdata = JSON.parse(atob(localStorage.getItem('token')));
    this.userDetail = localdata;
    this.userRole = this.userDetail.role;
        /* LENGUAGE */
        if(localStorage.getItem('lenguage')){
          if(localStorage.getItem('lenguage') === 'es'){
              this.flagImg = './assets/images/spanish_flag.png';
          }else{
              this.flagImg = './assets/images/uk_flag.png';
          }
          this.initTranslateDefault(localStorage.getItem('lenguage'));
        }else{
          this.getDeviceLanguage();
        }
  }

  _initialiseTranslation(): void {
    this._translate.get('DASHBOARD.projects').subscribe((res: string) => {
      //this.projectstitle = res;
    });
    this._translate.get('DASHBOARD.search').subscribe((res: string) => {
      //this.search = res;
    });

  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initTranslate(language: string) {
    if (language) {
      this.language = this._translate.getBrowserLang();
    }
    else {
      // Set your language here
      this.language = 'en';
    }
    this._translateLanguage();;
  }

  initTranslateDefault(language: string) {
    if (language) {
      this.language = language;
    }
    this._translateLanguage();;
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === 'object') {
      this._initTranslate(navigator.language);
    }
    else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value);
        })
        .catch(e => {
           // console.log(e);
          });
    }
  }

  ionViewWillEnter() {
    /* LENGUAGE */
    if(localStorage.getItem('lenguage')){
      this.initTranslateDefault(localStorage.getItem('lenguage'));
    }else{
      this.getDeviceLanguage();
    }
  }

  ionViewDidEnter(){
    if(localStorage.getItem('lenguage')){
      this.initTranslateDefault(localStorage.getItem('lenguage'));
    }else{
      this.getDeviceLanguage();
    }
  }


  switchLenguages(){
      const leng = localStorage.getItem('lenguage');
      console.log(leng);
      if(leng){
        switch(leng){
          case 'es': localStorage.setItem('lenguage','en');this.initTranslateDefault('en');window.location.reload(); break;
          case 'en': localStorage.setItem('lenguage','es');this.initTranslateDefault('es');window.location.reload(); break;
        }
      }else{
          localStorage.setItem('lenguage','en');this.initTranslateDefault('en');window.location.reload();
    }
  }
}
