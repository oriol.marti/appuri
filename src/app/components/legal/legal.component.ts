/* eslint-disable no-underscore-dangle */
import { Component, OnInit } from '@angular/core';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { CheckboxCustomEvent, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss'],
})
export class LegalComponent implements OnInit {


  public language: string;
  title: string;
  para1: string;
  para2: string;
  para3: string;
  para4: string;
  para5: string;
  para6: string;

  constructor(
    public modalController: ModalController,
    private globalization: Globalization,
    private _translate: TranslateService,
  ) {}

  async ngOnInit() {

  }

  ionViewDidEnter() {
    if(localStorage.getItem('lenguage')){
      this.initTranslateDefault(localStorage.getItem('lenguage'));
    }else{
      this.getDeviceLanguage();
    }
  }

  _initialiseTranslation(): void {
    this._translate.get('AVISOLEGAL.title').subscribe((res: string) => {
      this.title = res;
    });
    this._translate.get('AVISOLEGAL.para1').subscribe((res: string) => {
      this.para1 = res;
    });
    this._translate.get('AVISOLEGAL.para2').subscribe((res: string) => {
      this.para2 = res;
    });
    this._translate.get('AVISOLEGAL.para3').subscribe((res: string) => {
      this.para3 = res;
    });
    this._translate.get('AVISOLEGAL.para4').subscribe((res: string) => {
      this.para4 = res;
    });
    this._translate.get('AVISOLEGAL.para5').subscribe((res: string) => {
      this.para5 = res;
    });
    this._translate.get('AVISOLEGAL.para6').subscribe((res: string) => {
      this.para6 = res;
    });
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initTranslate(language: string) {
    if (language) {
      this.language = this._translate.getBrowserLang();
    }
    else {
      // Set your language here
      this.language = 'en';
    }
    this._translateLanguage();;
  }

  initTranslateDefault(language: string) {
    if (language) {
      this.language = language;
    }
    this._translateLanguage();;
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === 'object') {
      this._initTranslate(navigator.language);
    }
    else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value);
        })
        .catch(e => {
          // console.log(e);
        });
    }
  }

}
