import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';

import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { ReactiveFormsModule } from '@angular/forms';

import { ProjectDetailsComponent } from './project-details/project-details.component';
import { EditUserModalComponent } from './edit-user-modal/edit-user-modal.component';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { AuthGuard } from './guards/auth.guard';
import { NgxHideOnScrollModule } from 'ngx-hide-on-scroll';
import { SettingsComponent } from './settings/settings.component';

import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';

import { ErrorAlerter } from './errorhandler';

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions, @typescript-eslint/naming-convention
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  // eslint-disable-next-line max-len
  declarations: [AppComponent, ProjectDetailsComponent, EditUserModalComponent, SettingsComponent],
  entryComponents: [ProjectDetailsComponent,],
  // eslint-disable-next-line max-len
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, provideFirebaseApp(() => initializeApp(environment.firebaseConfig)), AngularFireModule.initializeApp(environment.firebaseConfig), AngularFireStorageModule, AngularFireDatabaseModule,
    // eslint-disable-next-line max-len
    AngularFireAuthModule, HttpClientModule, ReactiveFormsModule, NgxHideOnScrollModule, TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }), AngularFireStorageModule, AngularFirestoreModule
  ],
  providers: [Globalization, { provide: RouteReuseStrategy, useClass: IonicRouteStrategy, }, TranslateService, AuthGuard, EmailComposer,
    {
      provide: ErrorHandler,
      useClass: ErrorAlerter,
    },  //AQUI
    ],
  bootstrap: [AppComponent],
})
export class AppModule { }
