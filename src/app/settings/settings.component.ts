import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { ModalController, PopoverController, ToastController } from '@ionic/angular';
//import { TranslateService } from '@ngx-translate/core';
import { User } from '../interfaces/users';
import { IonicAuthService } from '../services/ionic-auth.service';
import { TermsyconditionsComponent } from '../components/termsyconditions/termsyconditions.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})


export class SettingsComponent implements OnInit {
  userDetail: User;
  public title: string;
  public description: string;
  public language: string;
  darkModeToggle = document.querySelector('#toggle-button');
  darkModel = localStorage.getItem('darkMode');

  constructor(public modalController: ModalController,
    private ionicAuthService: IonicAuthService,
    private router: Router,
    public toastController: ToastController,
    private popCtrl: PopoverController,
  ) {
    this.ionicAuthService.userDetails().subscribe(response => {
      if (response !== null) {
        const localdata = JSON.parse(atob(localStorage.getItem('token')));
        if (response.email === localdata.email) {
        } else {
          localStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }
      } else {
        this.router.navigateByUrl('login');
      }
    }, error => {
      // console.log(error);
    });
  }

  ngOnInit() {

    const localdata = JSON.parse(atob(localStorage.getItem('token')));
    this.userDetail = localdata;

    /*     if (this.darkModel !== 'enabled') {
          this.theme.enableDarkMode();
        } else {
          this.theme.disableDarkMode();
        } */

  }

  signOut() {
    this.ionicAuthService.signoutUser()
      .then(res => {
        this.router.navigateByUrl('login');
      })
      .catch(error => {
        // console.log(error);
      });
  }

  async canDismiss() {
    this.modalController.dismiss();
  }

  slcBTN() {
    this.router.navigateByUrl('lenguages');
  }

  async openPrivacyModal() {
    const modal = await this.modalController.create({
      component: TermsyconditionsComponent,
      breakpoints: [0, 0.75],
      componentProps: {},
      initialBreakpoint: 0.75,
    });
    await modal.present();

  }
}
