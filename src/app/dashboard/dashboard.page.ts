/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
import { AfterContentInit, AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute, ActivationStart, Route, Router, RouterOutlet } from '@angular/router';
import { ModalController, ToastController, MenuController, MenuCustomEvent, AnimationController, Platform, IonReorderGroup, ItemReorderEventDetail } from '@ionic/angular';
import { User } from '../interfaces/users';
import { IonicAuthService } from '../services/ionic-auth.service';
import { SettingsComponent } from '../settings/settings.component';
import { WebServiceService } from '../services/web-service.service';
import { ProjectDetailsComponent } from '../project-details/project-details.component';
import { LocalNotificationService } from '../services/local-notification.service';
import { ActionPerformed, PushNotificationSchema, PushNotifications, Token } from '@capacitor/push-notifications';
import { Capacitor } from '@capacitor/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { concatMap, delay, retryWhen } from 'rxjs/operators';
import { TermsyconditionsComponent } from '../components/termsyconditions/termsyconditions.component';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})

export class DashboardPage implements OnInit, HttpInterceptor, AfterViewInit {
  @ViewChild(IonReorderGroup) reorderGroup: IonReorderGroup;
  @ViewChild(RouterOutlet) outlet: RouterOutlet;
  @ViewChildren('ListItem', { read: ElementRef })
  listRef: QueryList<ElementRef>;
  userDetail: User;
  projects: any;
  errors: any = '';
  projectsArray: any[] = [];
  filterTerm: string;
  isPushNotificationsAvailable = Capacitor.isPluginAvailable('PushNotifications');
  projectsArrayQuery: any[] = [];
  userRole = 'user';
  preload = true;
  canceledProjects: any;
  activeProjects: any;
  allProjects: any;
  finishedProjects: any;
  retryCount = 3;
  role = 'client';
  loadingError: boolean;
  displayEmpty= false;

  slideOpts = {
    loop: false
  };

  link1css = 'link-activo';
  link2css = 'link-inactivo';
  link3css = 'link-inactivo';

  projectstitle: string;
  search: string;
  canceled: string;
  error: string;
  errortext: string;
  done: string;
  inprogress: string;
  inproduction: string;
  given: string;
  other: string;

  public lenguage: string;

  constructor(
    private router: Router,
    private ionicAuthService: IonicAuthService,
    public modalController: ModalController,
    private webservice: WebServiceService,
    public toastController: ToastController,
    private notification: LocalNotificationService,
    private animationCtrl: AnimationController,
    private platform: Platform,
    private globalization: Globalization,
    private _translate: TranslateService,

  ) {
    this.loadingError = false;
    this.ionicAuthService.userDetails().subscribe(response => {
      if (response !== null) {
        const localdata = JSON.parse(atob(localStorage.getItem('token')));
        if (response.email === localdata.email) {
        } else {

          localStorage.setItem('token', 'null');

/*           this.router.navigateByUrl('login').then(() => {
            // this.loadingService.hideLoader();
            window.location.reload();
          }); */

          return 1;
        }
      } else {

        /* this.router.navigateByUrl('login').then(() => {
          window.location.reload();
        }); */
      }
    }, error => {
      console.error(error);
    });
  }
  ngOnInit() {
    // console.log('Initializing HomePage');
    /* LENGUAGE */
    if(localStorage.getItem('lenguage')){
      this.initTranslateDefault(localStorage.getItem('lenguage'));
    }else{
      this.getDeviceLanguage();
    }

    if (this.isPushNotificationsAvailable) {
      this.initPushNotifications();
    }

    if (localStorage.getItem('token') === 'null' || !localStorage.getItem('token')) {

      this.ionicAuthService.signoutUser().then(()=>{this.router.navigate(['login']);});

    } else {
      const localdata = JSON.parse(atob(localStorage.getItem('token')));
      this.userDetail = localdata;
      this.userRole = this.userDetail.role;
    };
    setTimeout(async () => {

      this.addProjects();

    }, 5000);

    this.role = this.userDetail.role;
    // console.log(this.loadingError);
  }

  /**
   * We add the projects to the main Array.
   *
   * @uri
   */
  async addProjects() {
    this.getRetrieveData(this.userDetail.linkAccount);
    // console.log('fuera');
    // console.log(this.userDetail.assignedProjects);
    // console.log(this.userDetail);
    this.userDetail.assignedProjects.forEach((element: any) => {this.getdata(element);});

    setTimeout(() => {
      this.projectsArrayQuery = this.projectsArrayQuery.sort((a, b) => {
        if (a.project_no > b.project_no) {
          return -1;
        } else if (a.project_no < b.project_no) {
          return 1;
        } else {
          return 0;
        }
      });
      this.preload = false;
    }, 1000);
  }

  /**
   * Executes once u enter the view
   *
   * @uri
   */
  ngAfterViewInit() {

    // Workaround just to fix list flicker issue especially on Android
    setTimeout(
      () => this.initListAnimation(),
      this.platform.is('android') ? 500 : 0
    );
    // this.initListAnimation();
  }

  /**
   * Adding animations to the list
   *
   * @uri
   */
  initListAnimation() {
    const itemRefArray = this.listRef.toArray();
    for (let i = 0; i < itemRefArray.length; i++) {
      const element = itemRefArray[i].nativeElement;

      this.animationCtrl
        .create()
        .addElement(element)
        .duration(1000)
        .delay(i * (1000 / 3))
        .easing('cubic-bezier(0.4, 0.0, 0.2, 1.0)')
        .fromTo('transform', 'translateY(50px)', 'translateY(0px)')
        .fromTo('opacity', '0', '1')
        .play();
    }
  }

  /**
   * Push of Firebase notifications
   *
   * @uri
   */
  initPushNotifications() {
    PushNotifications.requestPermissions().then(result => {
      if (result.receive === 'granted') {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
      }
    });

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener('pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        /* alert('Push received: ' + JSON.stringify(notification)); */
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
      (notification: ActionPerformed) => {
        /* alert('Push action performed: ' + JSON.stringify(notification)); */
      }
    );
  }

  /**
   * Dom creates modal with ionic modalcontroller
   *
   * @uri
   */
  async presentModal() {
    const modal = await this.modalController.create({
      component: SettingsComponent,
      breakpoints: [0, 1],
      componentProps: {},

    });
    await modal.present();
  }

  /**
   * Get retrieve api data from a project id
   * param1: iproject: string of numbers
   *
   * @uri
   */
  async getdata(idproject) {

  }

  /**
   * Get retrieve api data from a query
   *
   * @uri
   */
  async getRetrieveData(linkAccount) {
    // eslint-disable-next-line max-len
    this.webservice.retrieveQuery('query=SELECT * FROM Project WHERE linktoaccountscontacts = ' + linkAccount + ';').subscribe(async (result) => {
      if (!result.success) {
        this.errors = result.error.message;
        console.error(this.errors);
      } else {
        //eliminamos todos los proyectos cancelados
        this.canceledProjects = result.result.filter(project => project.projectstatus === 'Cancelado');
        this.projectsArrayQuery = result.result.filter(project => project.projectstatus !== 'Cancelado');
        this.allProjects = this.projectsArrayQuery;
        this.activeProjects = this.projectsArrayQuery.filter(project => project.projectstatus !== '5- Finalizado' && project.projectstatus !== 'Cancelado' );
        this.finishedProjects = this.projectsArrayQuery.filter(project => project.projectstatus === '5- Finalizado');

        this.projectsArrayQuery = this.projectsArrayQuery.sort((a, b) => {
          if (a.project_no > b.project_no) {
            return -1;
          } else if (a.project_no < b.project_no) {
            return 1;
          } else {
            return 0;
          }
        });
      }
    });
/*     setTimeout(() => {
      this.preload = false;
    }, 300); */
  }

  /**
   * Dom creates the project detail modal
   *
   * @uri
   */
  async presentprojectModal(id, nombre, description, status, createdtime, cagent, planos, construc, project, fairsandeventsheaderid) { //montar atributos a mostrar
    const modal = await this.modalController.create({
      component: ProjectDetailsComponent,
      componentProps: { id, nombre, description, status, createdtime, cagent, planos, construc, project, fairsandeventsheaderid },
      cssClass: 'modal-fullscreen',
    });
    await modal.present();
  }

  sendLocalNotificationComents() {
    this.notification.showLocalNotification(1, ' nuevo commentario ', 'nuevo comentario añadido a su proyecto');
  }

  sendLocalNotificationProjectChanges() {
    // console.log('Project has changed');
    this.notification.showLocalNotification(1, ' Cambios en su proyecto ', ' Hay nuevos cambios sobre su proyecto ');
  }

  async openPrivacyModal() {
    const modal = await this.modalController.create({
      component: TermsyconditionsComponent,
      breakpoints: [0, 0.75],
      componentProps: {},
      initialBreakpoint: 0.75,
    });
    await modal.present();

  }

  /**
   * Refreshes the list of projects.
   * Param1: event: event
   *
   * @uri
   */
  doRefresh(event) {
    // console.log('refresh');
    setTimeout(() => {
      this.addProjects();
      event.target.complete();
      if (event.cancelable) { event.preventDefault(); }
    }, 4000);
  }

  /**
   * If the connection fails the request will be retried 3 times.
   *
   *  Param1:Http Request,
   *  Param2:Next http request on row
   *
   * @uri
   */
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      retryWhen(error =>
        error.pipe(
          // eslint-disable-next-line @typescript-eslint/no-shadow
          concatMap((error, count) => {
            if (count <= this.retryCount && error.status === 503) {
              return of(error);
            }
            return throwError(error);
          }),
          delay(1000)
        )
      )
    );
  }


  ionViewDidEnter() {
    /* LENGUAGE */
    if(localStorage.getItem('lenguage')){
      this.initTranslateDefault(localStorage.getItem('lenguage'));
    }else{
      this.getDeviceLanguage();
    }
}

  _initialiseTranslation(): void {
    this._translate.get('DASHBOARD.projects').subscribe((res: string) => {
      this.projectstitle = res;
    });
    this._translate.get('DASHBOARD.search').subscribe((res: string) => {
      this.search = res;
    });
    this._translate.get('DASHBOARD.canceled').subscribe((res: string) => {
      this.canceled = res;
    });
    this._translate.get('DASHBOARD.error').subscribe((res: string) => {
      this.error = res;
    });
    this._translate.get('DASHBOARD.errortext').subscribe((res: string) => {
      this.errortext = res;
    });
    this._translate.get('DASHBOARD.done').subscribe((res: string) => {
      this.done = res;
    });
    this._translate.get('DASHBOARD.inprogress').subscribe((res: string) => {
      this.inprogress = res;
    });
    this._translate.get('DASHBOARD.inproduction').subscribe((res: string) => {
      this.inproduction = res;
    });
    this._translate.get('DASHBOARD.given').subscribe((res: string) => {
      this.given = res;
    });
    this._translate.get('DASHBOARD.other').subscribe((res: string) => {
      this.other = res;
    });
  }

  _translateLanguage(): void {
    this._translate.use(this.lenguage);
    this._initialiseTranslation();
  }

  _initTranslate(lenguage: string) {
    if (lenguage) {
      this.lenguage = this._translate.getBrowserLang();
    }
    else {
      // Set your lenguage here
      this.lenguage = 'en';
    }
    this._translateLanguage();;
  }

  initTranslateDefault(lenguage: string) {
    if (lenguage) {
      this.lenguage = lenguage;

    }
    this._translateLanguage();;
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === 'object') {
      this._initTranslate(navigator.language);
    }
    else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value);
        })
        .catch(e => {
          // console.log(e);
          });
    }
  }

  doReorder(ev: CustomEvent<ItemReorderEventDetail>) {
    this.projectsArrayQuery = ev.detail.complete(this.projectsArrayQuery);
  }

  togglelink(link: number) {
/*     this.allProjects = this.projectsArrayQuery;
    this.activeProjects = this.projectsArrayQuery.filter(project => project.projectstatus !== '5- Finalizado' && project.projectstatus !== 'Cancelado' );
    this.finishedProjects */

    if (link === 1) {
      this.link1css = 'link-activo';
      this.link2css = 'link-inactivo';
      this.link3css = 'link-inactivo';
      this.projectsArrayQuery = this.allProjects;
      this.checkIfEmpty();
    } else if (link === 2) {
      this.link1css = 'link-inactivo';
      this.link2css = 'link-activo';
      this.link3css = 'link-inactivo';
      this.projectsArrayQuery = this.activeProjects;
      this.checkIfEmpty();

    } else if (link === 3) {
      this.link1css = 'link-inactivo';
      this.link2css = 'link-inactivo';
      this.link3css = 'link-activo';
      this.projectsArrayQuery = this.finishedProjects;
      this.checkIfEmpty();
    }

  }

  checkIfEmpty(){
    if(this.projectsArrayQuery.length > 0){
      this.displayEmpty = false;
    }else{
      this.displayEmpty = true;
    }
  }

  reload(){
    window.location.reload();
  }
}

