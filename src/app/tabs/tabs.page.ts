/* eslint-disable max-len */
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  icon1 = './assets/images/gprojects.png';
  icon2 = './assets/images/homemenu_filled.png';
  icon3 = './assets/images/userprofile.png';
  selected1: boolean;
  selected2: boolean;
  selected3: boolean;

  constructor(
    private router: Router,
  ) {
    this.selected1 = true;
    this.selected2 = false;
    this.selected3 = false;
    if (this.router.url === '/tabs/tab1') {
      this.toggleIcon(2);

    } else if (this.router.url === '/tabs/tab2') {
      this.toggleIcon(1);

    } else if (this.router.url === '/tabs/tab3') {
      this.toggleIcon(3);

    }
  }

  toggleIcon(inconN: number) {
    const project = './assets/images/gprojects.png';
    const projectFilled = './assets/images/gprojects_filled.png';

    const hmenu = './assets/images/homemenu.png';
    const hmenuFilled = './assets/images/homemenu_filled.png';

    const user = './assets/images/userprofile.png';
    const userFilled = './assets/images/userprofile_filled.png';

    if (inconN === 2) {
      this.icon1 = projectFilled;
      this.icon2 = hmenu;
      this.icon3 = user;
      this.selected1 = false;
      this.selected2 = true;
      this.selected3 = false;
    } else if (inconN === 1) {
      this.icon2 = hmenuFilled;
      this.icon1 = project;
      this.icon3 = user;
      this.selected1 = true;
      this.selected2 = false;
      this.selected3 = false;
    } else if (inconN === 3) {
      this.icon2 = hmenu;
      this.icon1 = project;
      this.icon3 = userFilled;
      this.selected1 = false;
      this.selected2 = false;
      this.selected3 = true;
    }
  }

}
