import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleselectorPage } from './roleselector.page';

const routes: Routes = [
  {
    path: '',
    component: RoleselectorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleselectorPageRoutingModule {}
