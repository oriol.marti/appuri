import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoleselectorPageRoutingModule } from './roleselector-routing.module';

import { RoleselectorPage } from './roleselector.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoleselectorPageRoutingModule
  ],
  declarations: [RoleselectorPage]
})
export class RoleselectorPageModule {}
