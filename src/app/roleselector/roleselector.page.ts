import { Component, OnInit } from '@angular/core';
import { AuthStorageService } from '../services/auth-storage.service';
import { User } from '../interfaces/users';
import { MenuController, ModalController } from '@ionic/angular';
import { EditUserModalComponent } from '../edit-user-modal/edit-user-modal.component';
import { IonicAuthService } from '../services/ionic-auth.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { SettingsComponent } from '../settings/settings.component';

@Component({
  selector: 'app-roleselector',
  templateUrl: './roleselector.page.html',
  styleUrls: ['./roleselector.page.scss'],
})
export class RoleselectorPage implements OnInit {
  userDetail: User;
  users = [];
  // eslint-disable-next-line @typescript-eslint/naming-convention
  UserData: User = {} as User;

  constructor(private dataService: AuthStorageService,
    public modalController: ModalController,
    private ionicAuthService: IonicAuthService,
    public router: Router,
    // eslint-disable-next-line @typescript-eslint/naming-convention, @typescript-eslint/no-shadow
    public Location: Location,
  ) {
    this.ionicAuthService.userDetails().subscribe(response => {
      if (response !== null) {
        const localdata = JSON.parse(atob(localStorage.getItem('token')));
        if (response.email === localdata.email) {
        } else {
          localStorage.setItem('token', 'null');
          this.ionicAuthService.signoutUser().then(()=>{this.router.navigate(['login']);});
        }
      } else {
        this.ionicAuthService.signoutUser().then(()=>{this.router.navigate(['login']);});
      }
    }, error => {
      console.log(error);
    });
  }
  async ngOnInit() {
    const localdata = JSON.parse(atob(localStorage.getItem('token')));
    this.userDetail = localdata;

    this.dataService.getUsers().subscribe(
      res => {
        this.users = res.map((item) => ({
          id: item.payload.doc.id,
          ...item.payload.doc.data() as any
        }));
      }
    );

  }
  async presentEditModal() {
    const modal = await this.modalController.create({
      component: EditUserModalComponent,
      componentProps: {},

    });
    await modal.present();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SettingsComponent,
      breakpoints: [0, 1],
      componentProps: {},

    });
    await modal.present();
  }

  lastPage() {
    this.Location.back();
  }

  routerRedirect(urlroute: string) {
    this.router.navigateByUrl(urlroute).then(() => {
      window.location.reload();
    });
  }

  adduser() {
    this.router.navigateByUrl('register');
  }


}
