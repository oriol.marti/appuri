import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [

  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
    canLoad: [AuthGuard]
  },
   {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
/*   {
    path: '**',
    redirectTo: 'login'
  }, */
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule),
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'roleselector',
    loadChildren: () => import('./roleselector/roleselector.module').then( m => m.RoleselectorPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'dextail-services',
    loadChildren: () => import('./dextail-services/dextail-services.module').then( m => m.DextailServicesPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'email',
    loadChildren: () => import('./email/email.module').then( m => m.EmailPageModule),
    canLoad: [AuthGuard]
  },

];

@NgModule({
  imports: [
  RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
