
export interface Comment {
  commentId: any;
  projectId: any;
  userImg: any;
  userId: any;
  username: any;
  comment: string;
  lastEdit: any;
}
