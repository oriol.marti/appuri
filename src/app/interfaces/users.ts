export interface User {
  email: string;
  displayName: string;
  role: string;
  brand: string;
  linkAccount: string;
  assignedProjects: any;
  img: string;
}
