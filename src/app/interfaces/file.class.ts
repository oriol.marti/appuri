export interface FILE {
  name: string;
  filepath: string;
  idItem: any;
  size: number;
}
