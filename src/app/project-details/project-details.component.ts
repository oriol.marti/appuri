/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-underscore-dangle */
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {  AlertController, IonAccordionGroup, ModalController, ToastController } from '@ionic/angular';
import { User } from '../interfaces/users';
import { Comment } from '../interfaces/comment';
import { IonicAuthService } from '../services/ionic-auth.service';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CommentsService } from '../services/comments.service';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { WebServiceService } from '../services/web-service.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss'],
})

export class ProjectDetailsComponent implements OnInit, OnDestroy {

  @ViewChild('accordionGroup', { static: true }) accordionGroup: IonAccordionGroup;

  @Input() id: string;
  @Input() nombre: string;
  @Input() description: string;
  @Input() status: string;
  @Input() createdtime: string;
  @Input() cagent: string;
  @Input() planos: string;
  @Input() construc: string;
  @Input() project: any;
  @Input() fairsandeventsheaderid: any;
  userDetail: User;
  commentForm: FormGroup;
  successMsg = '';
  errorMsg = '';
  commentsObj: any;
  commentsArray: any;
  errors: string;
  language: string;
  feria: any;
  statusvar=0;

  date_start: string;
  statust: string;
  date_end: string;
  planost:  string;
  Constructivo: string;
  contact: string;
  create: string;
  ended: string;
  other: string;
  pendant: string;
  location: string;
  startfairdate: string;
  endfairdate: string;
  pendantaprobeprice: string;
  blueprintinprogrest: string;
  aprobed: string;
  bleupritnsinproces: string;
  notype: string;
  hiring: string;
  water: string;
  hire: string;
  ispaid: string;
  done: string;
  canon: string;
  paid: string;
  confirmed: string;
  inprogrest: string;
  intramit: string;
  limitdate: string;
  comprimedair: string;
  electricity: string;
  needed: string;
  designs: string;
  commentst: string;
  lastedit: string;
  nocontract: string;
  graphicarts: string;
  endarts: string;
  hiringalerttext: string;
  wateralerttext: string;
  canonalerttext: string;
  electalerttext: string;
  graphartsalerttext: string;
  compairalerttext: string;
  riggingalerttext: string;
  errorProvide: string;
  comentsplaceholder: string;

  alertcon: string;
  alertwater: string;
  alertcanon: string;
  alertair: string;
  alertelect: string;
  alertrigging: string;
  aertart: string;

  handlerMessage = '';
  roleMessage = '';
  titletext='';
  alertext= '';

  // eslint-disable-next-line @typescript-eslint/naming-convention
  error_msg = {
    comment: [
      {
        type: 'required',
        message_en: 'Provide a message',
        message_es: 'Introduce un mensage',
      },
    ]
  };

  constructor(
    private modalController: ModalController,
    private ionicAuthService: IonicAuthService,
    private router: Router,
    private afStore: AngularFirestore,
    private fb: FormBuilder,
    private comments: CommentsService,
    private toastController: ToastController,
    private globalization: Globalization,
    private _translate: TranslateService,
    private webservice: WebServiceService,
    private alertController: AlertController,

  ) {
    this.language = localStorage.getItem('lenguage');
  }
  ngOnDestroy(): void {
    if (!this.commentsArray == null) { window.location.reload(); }
  }

  async canDismiss() {
    this.modalController.dismiss();
  }

  ngOnInit() {
    const localdata = JSON.parse(atob(localStorage.getItem('token')));
    this.userDetail = localdata;
    this.commentForm = this.fb.group({
      comment: new FormControl('', Validators.compose([Validators.required]))
    });

    //this.commentsObj = this.comments.getFilteredComents(this.id);
    this.comments.getCommentsProject(this.id).subscribe(
      res => {
        this.commentsArray = res.map((item) => ({
          id: item.payload.doc.id,
          ...item.payload.doc.data() as Comment
        }));
      }
    );

    this.status = this.status.slice(2);
    this.planos = this.planos.slice(2);
    this.construc = this.construc.slice(2);

    switch(this.status){
      case ' Pendiente Agencia':this.statusvar = 0.1;
        break;
      case ' Pendiente traspaso':this.statusvar = 0.3;
        break;
      case ' En produccion':this.statusvar = 0.4;
        break;
      case ' Produccion finalizada':this.statusvar = 0.5;
        break;
      case ' Entregado':this.statusvar = 0.9;
        break;
      case ' Finalizado':this.statusvar = 1;
        break;
      case ' Cancelado':this.statusvar = 0.5;
        break;
    }
        /* LENGUAGE */
        if(localStorage.getItem('lenguage')){
          this.initTranslateDefault(localStorage.getItem('lenguage'));
        }else{
          this.getDeviceLanguage();
        }
  }

  setCommentData(comment) {
    const commentId = this.makeid(10);
    const currentdate = new Date();
    const datetime = '' + currentdate.getDate() + '/'
      + (currentdate.getMonth() + 1) + '/'
      + currentdate.getFullYear() + ' || '
      + currentdate.getHours() + ':'
      + currentdate.getMinutes() + ':'
      + currentdate.getSeconds();
    const commentRef: AngularFirestoreDocument<any> = this.afStore.doc(`comments/${commentId}`);
    const commentData: Comment = {
      // eslint-disable-next-line object-shorthand
      commentId: commentId,
      projectId: this.id,
      userImg: this.userDetail.img,
      userId: this.userDetail.email,
      username: this.userDetail.displayName,
      comment: comment.comment,
      lastEdit: datetime,
    };
    this.displayToast();
    this.commentForm.reset();
    return commentRef.set(commentData);
  }

  makeid(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() *
        charactersLength));
    }
    return result;
  }

  displayToast() {


    try {
      this.toastController.dismiss().then(() => {
      }).catch(() => {
      }).finally(() => {
        console.log('Closed');
      });
    } catch (e) { }

    this.toastController.create({
      message: 'Comentario añadido',
      position: 'top',
      duration: 2000,
      animated: true,
      cssClass: 'toast-custom-class',
      buttons: [
        {
          side: 'end',
          text: 'Cerrar',
          role: 'cancel',
          handler: () => {
            console.log('Comment added');
          }
        }
      ]
    }).then((toast) => {
      toast.present();
    });
  }

  sendEmail() {
    this.router.navigateByUrl('email').then(() => {
      this.modalController.dismiss();
    });
    //this.toast('middle', 'Error al enviar el mail, vuelva  aintentarlo');
  }

  async toast(position: 'top' | 'middle' | 'bottom', massage: string) {
    const toast = await this.toastController.create({
      // eslint-disable-next-line max-len
      message: massage,
      duration: 1500,
      // eslint-disable-next-line object-shorthand
      position: position,
      cssClass: 'custom_toast',
    });

    await toast.present();
  }

  async presentAlert(title: string, text: string) {
    const alert = await this.alertController.create({
      header: title,
      message: text,
      buttons: [
        {
          text: 'OK!',
          role: 'Etendido',
          handler: () => {
            this.handlerMessage = 'Alert confirmed';
          },
        },
      ],
    });

    await alert.present();
    const { role } = await alert.onDidDismiss();
    this.roleMessage = `Dismissed with role: ${role}`;
  }


  ionViewDidEnter() {
    /* LENGUAGE */
    if(localStorage.getItem('lenguage')){
      this.initTranslateDefault(localStorage.getItem('lenguage'));
    }else{
      this.getDeviceLanguage();
    }
  }

  _initialiseTranslation(): void {
    this._translate.get('PROJECT_DETAILS.date_start').subscribe((res: string) => {
      this.date_start = res;
    });
    this._translate.get('PROJECT_DETAILS.status').subscribe((res: string) => {
      this.statust = res;
    });
    this._translate.get('PROJECT_DETAILS.date_end').subscribe((res: string) => {
      this.date_end = res;
    });
    this._translate.get('PROJECT_DETAILS.planos').subscribe((res: string) => {
      this.planost = res;
    });
    this._translate.get('PROJECT_DETAILS.Constructivo').subscribe((res: string) => {
      this.Constructivo = res;
    });
    this._translate.get('PROJECT_DETAILS.contact').subscribe((res: string) => {
      this.contact = res;
    });
    this._translate.get('PROJECT_DETAILS.create').subscribe((res: string) => {
      this.create = res;
    });
    this._translate.get('PROJECT_DETAILS.ended').subscribe((res: string) => {
      this.ended = res;
    });
    this._translate.get('PROJECT_DETAILS.other').subscribe((res: string) => {
      this.other = res;
    });
    this._translate.get('PROJECT_DETAILS.pendant').subscribe((res: string) => {
      this.pendant = res;
    });
    this._translate.get('PROJECT_DETAILS.location').subscribe((res: string) => {
      this.location = res;
    });
    this._translate.get('PROJECT_DETAILS.startfairdate').subscribe((res: string) => {
      this.startfairdate = res;
    });
    this._translate.get('PROJECT_DETAILS.endfairdate').subscribe((res: string) => {
      this.endfairdate = res;
    });
    this._translate.get('PROJECT_DETAILS.pendantaprobeprice').subscribe((res: string) => {
      this.pendantaprobeprice = res;
    });
    this._translate.get('PROJECT_DETAILS.blueprintinprogrest').subscribe((res: string) => {
      this.blueprintinprogrest = res;
    });
    this._translate.get('PROJECT_DETAILS.aprobed').subscribe((res: string) => {
      this.aprobed = res;
    });
    this._translate.get('PROJECT_DETAILS.bleupritnsinproces').subscribe((res: string) => {
      this.bleupritnsinproces = res;
    });
    this._translate.get('PROJECT_DETAILS.notype').subscribe((res: string) => {
      this.notype = res;
    });
    this._translate.get('PROJECT_DETAILS.hiring').subscribe((res: string) => {
      this.hiring = res;
    });
    this._translate.get('PROJECT_DETAILS.water').subscribe((res: string) => {
      this.water = res;
    });
    this._translate.get('PROJECT_DETAILS.hire').subscribe((res: string) => {
      this.hire = res;
    });
    this._translate.get('PROJECT_DETAILS.ispaid').subscribe((res: string) => {
      this.ispaid = res;
    });
    this._translate.get('PROJECT_DETAILS.done').subscribe((res: string) => {
      this.done = res;
    });
    this._translate.get('PROJECT_DETAILS.canon').subscribe((res: string) => {
      this.canon = res;
    });
    this._translate.get('PROJECT_DETAILS.paid').subscribe((res: string) => {
      this.paid = res;
    });
    this._translate.get('PROJECT_DETAILS.confirmed').subscribe((res: string) => {
      this.confirmed = res;
    });
    this._translate.get('PROJECT_DETAILS.inprogress').subscribe((res: string) => {
      this.inprogrest = res;
    });
    this._translate.get('PROJECT_DETAILS.intramit').subscribe((res: string) => {
      this.intramit = res;
    });
    this._translate.get('PROJECT_DETAILS.limitdate').subscribe((res: string) => {
      this.limitdate = res;
    });
    this._translate.get('PROJECT_DETAILS.comprimedair').subscribe((res: string) => {
      this.comprimedair = res;
    });
    this._translate.get('PROJECT_DETAILS.electricity').subscribe((res: string) => {
      this.electricity = res;
    });
    this._translate.get('PROJECT_DETAILS.needed').subscribe((res: string) => {
      this.needed = res;
    });
    this._translate.get('PROJECT_DETAILS.designs').subscribe((res: string) => {
      this.designs = res;
    });
    this._translate.get('PROJECT_DETAILS.comments').subscribe((res: string) => {
      this.commentst = res;
    });
    this._translate.get('PROJECT_DETAILS.lastedit').subscribe((res: string) => {
      this.lastedit = res;
    });
    this._translate.get('PROJECT_DETAILS.nocontract').subscribe((res: string) => {
      this.nocontract = res;
    });
    this._translate.get('PROJECT_DETAILS.graphicarts').subscribe((res: string) => {
      this.graphicarts = res;
    });
    this._translate.get('PROJECT_DETAILS.endarts').subscribe((res: string) => {
      this.endarts = res;
    });
    this._translate.get('PROJECT_DETAILS.hiringalerttext').subscribe((res: string) => {
      this.hiringalerttext = res;
    });
    this._translate.get('PROJECT_DETAILS.wateralerttext').subscribe((res: string) => {
      this.wateralerttext = res;
    });
    this._translate.get('PROJECT_DETAILS.canonalerttext').subscribe((res: string) => {
      this.canonalerttext = res;
    });
    this._translate.get('PROJECT_DETAILS.electalerttext').subscribe((res: string) => {
      this.electalerttext = res;
    });
    this._translate.get('PROJECT_DETAILS.graphartsalerttext').subscribe((res: string) => {
      this.graphartsalerttext = res;
    });
    this._translate.get('PROJECT_DETAILS.compairalerttext').subscribe((res: string) => {
      this.compairalerttext = res;
    });
    this._translate.get('PROJECT_DETAILS.riggingalerttext').subscribe((res: string) => {
      this.riggingalerttext = res;
    });
    this._translate.get('PROJECT_DETAILS.comentsplaceholder').subscribe((res: string) => {
      this.comentsplaceholder = res;
    });
    }

    _translateLanguage(): void {
      this._translate.use(this.language);
      this._initialiseTranslation();
    }

    _initTranslate(language: string) {
      if (language) {
        this.language = this._translate.getBrowserLang();
      }
      else {
        // Set your language here
        this.language = 'en';
      }
      this._translateLanguage();;
    }

    initTranslateDefault(language: string) {
      //console.log(language);
      if (language) {
        this.language = language;
      }
      this._translateLanguage();;
    }

    getDeviceLanguage() {
      if (window.Intl && typeof window.Intl === 'object') {
        this._initTranslate(navigator.language);
      }
      else {
        this.globalization.getPreferredLanguage()
          .then(res => {
            this._initTranslate(res.value);
          })
          .catch(e => {
            //console.log(e);
           });
      }
    }
}
