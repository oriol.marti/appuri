/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonicAuthService } from '../services/ionic-auth.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from '../services/loading.service';
import { Platform } from '@ionic/angular';
import { dispatch } from 'rxjs/internal/observable/pairs';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
  userForm: FormGroup;
  successMsg = '';
  errorMsg = '';
  public title: string;
  public mail: string;
  public pass: string;
  public submit_btn: string;
  public reg_des: string;
  public register: string;
  public language: string;
  public passstring: string;

  private subscription: any;

  // eslint-disable-next-line @typescript-eslint/member-ordering
  error_msg = {
    email: [
      {
        type: 'required',
        message_es: 'Introduzca un email.',
        message_en: 'Provide an email'
      },
      {
        type: 'pattern',
        message_es: 'El email no es valido.',
        message_en: 'Email is not valid'
      }
    ],
    password: [
      {
        type: 'required',
        message_es: 'Introduza una contraseña valida.',
        message_en: 'Provide a valid password'
      },
      {
        type: 'minlength',
        message_es: 'La contraseña tiene qe ser almenos de 6 caracteres.',
        message_en: 'The password has to contain atleast 6 characters'
      }
    ]
  };

  constructor(
    private router: Router,
    private ionicAuthService: IonicAuthService,
    private fb: FormBuilder,
    private globalization: Globalization,
    private _translate: TranslateService,
    private loadingService: LoadingService,
    private platform: Platform,
  ) {
    this.ionicAuthService.signoutUser();
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
    });
  }

  signIn(value) {
    this.ionicAuthService.signinUser(value)
      .then((response) => {
        this.errorMsg = '';
        this.loadingService.showLoader().then(async () => {
          if (response) {
            setTimeout(() => {
              // console.log(localStorage.getItem('token'));
              if (localStorage.getItem('token') === 'null') {
                // console.log('primer if');
                const token = localStorage.getItem('idToken');
                this.ionicAuthService.signinUser(value);
              } else {
                // console.log('else');
                this.router.navigateByUrl('/tabs/tab2').then(() => { this.loadingService.hideLoader(); });
                return true;
              }
            }, 500);
          }
        });
      }, error => {
        //this.errorMsg = error.message;
        this.errorMsg = 'Error al iniciar sessión, porfavor vuelva a intentarlo.';
        this.successMsg = '';
      });
  }

  ionViewDidEnter() {
        /* LENGUAGE */
        if(localStorage.getItem('lenguage')){
          this.initTranslateDefault(localStorage.getItem('lenguage'));
        }else{
          this.getDeviceLanguage();
        }

        this.subscription = this.platform.backButton.subscribeWithPriority(9999, () => {
          // do nothing
        });
  }

  _initialiseTranslation(): void {
    this._translate.get('LOGIN.TITLE_LOGIN').subscribe((res: string) => {
      this.title = res;
    });
    this._translate.get('LOGIN.mail').subscribe((res: string) => {
      this.mail = res;
    });
    this._translate.get('LOGIN.pass').subscribe((res: string) => {
      this.pass = res;
    });
    this._translate.get('LOGIN.submit_login').subscribe((res: string) => {
      this.submit_btn = res;
    });
    this._translate.get('LOGIN.register_description').subscribe((res: string) => {
      this.reg_des = res;
    });
    this._translate.get('LOGIN.register').subscribe((res: string) => {
      this.register = res;
    });
    this._translate.get('LOGIN.passstring').subscribe((res: string) => {
      this.passstring = res;
    });
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initTranslate(language: string) {
    if (language) {
      this.language = this._translate.getBrowserLang();
    }
    else {
      // Set your language here
      this.language = 'en';
    }
    this._translateLanguage();;
  }

  initTranslateDefault(language: string) {
    if (language) {
      this.language = language;
    }
    this._translateLanguage();;
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === 'object') {
      this._initTranslate(navigator.language);
    }
    else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value);
        })
        .catch(e => {
          // console.log(e);
        });
    }
  }
}
