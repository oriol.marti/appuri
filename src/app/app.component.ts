/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, enableProdMode } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
//import { SplashScreen } from '@capacitor/splash-screen';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { app } from 'src/environments/environment';
import { FirebaseApp, FirebaseApps } from '@angular/fire/app';
import { SplashScreen } from '@capacitor/splash-screen';
import { LocalNotifications } from '@capacitor/local-notifications';
import { getAuth, User } from 'firebase/auth';
import { IonicAuthService } from './services/ionic-auth.service';
import * as firebase from 'firebase/compat';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { NetworkstatusService } from './services/networkstatus.service';
import { App, App as CapacitorApp } from '@capacitor/app';

//import { StatusBar } from '@ionic-native/status-bar/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  auth = getAuth();
  user = this.auth.currentUser;
  darkModel = localStorage.getItem('theme');
  speakers: any;

  constructor(
    private angularFireAuth: AngularFireAuth,
    private platform: Platform,
    public router: Router,
    private translate: TranslateService,
    public menu: MenuController,
    private ionicAuthService: IonicAuthService,
    private _translate: TranslateService,
    private connectivity: NetworkstatusService,
  ) {
    enableProdMode();
    SplashScreen.show({
      autoHide: false,
    });
    this.connectivity.appIsOnline$.subscribe((online) => {
      console.log('is online:' + online);
      if (online) {
        this.initializeApp();
      } else {
        this.initializeApp();
        console.log('App is offline');
        //route hacia una pagina de no tener conexion i mantener en stand by, con opcion
        //de cerrar app o refrescar el NetworkStatus
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      if(!localStorage.getItem('first')){
          localStorage.setItem('first', 'true');
          //this.router.navigate(['login']);
      }
      if(!localStorage.getItem('lenguage')){
          if(this._translate.getBrowserLang()){
              localStorage.setItem('lenguage', this._translate.getBrowserLang());
          }else{localStorage.setItem('lenguage','en');}
      }
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      app.automaticDataCollectionEnabled;

      this.translate.setDefaultLang(localStorage.getItem('lenguage'));

      this.checkAuth();

      SplashScreen.hide();

      App.addListener('backButton', ({canGoBack}) => {
        if(!canGoBack){
          App.exitApp();
        } else {
          window.history.back();
        }
      });
    });
  }
  private checkAuth() {
        if (!localStorage.getItem('token')) {
          this.router.navigate(['login']);
        }
  }
}
