import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonicAuthService } from '../services/ionic-auth.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})

export class RegisterPage implements OnInit {
  admin: any;
  project: any;
  client: any;

  userForm: FormGroup;
  successMsg = '';
  errorMsg = '';

  // eslint-disable-next-line @typescript-eslint/naming-convention
  error_msg = {
    displayName: [
      {
        type: 'required',
        message: 'Provide name.'
      },
      {
        type: 'minlength',
        message: 'name is not valid.'
      }
    ],
    email: [
      {
        type: 'required',
        message: 'Provide email.'
      },
      {
        type: 'pattern',
        message: 'Email is not valid.'
      }
    ],
    password: [
      {
        type: 'required',
        message: 'Password is required.'
      },
      {
        type: 'minlength',
        message: 'Password length should be 6 characters long.'
      }
    ],
    role: [
      {
        type: 'required',
        message: 'Select a role.'
      }
    ],
    brand: [{
      type: 'required',
      message: 'select brand.'
    }]
  };

  constructor(
    private router: Router,
    private ionicAuthService: IonicAuthService,
    private fb: FormBuilder
  ) {
    this.ionicAuthService.userDetails().subscribe(response => {
      if (response !== null) {
        const localdata = JSON.parse(atob(localStorage.getItem('token')));
        if (response.email === localdata.email) {
        } else {
          this.ionicAuthService.signoutUser().then(()=>{this.router.navigate(['login']);});
        }
      } else {
        this.ionicAuthService.signoutUser().then(()=>{this.router.navigate(['login']);});
      }
    }, error => {
      console.log(error);
    });

    /*   var secondaryApp = firebase.initializeApp(config, "Secondary");
      secondaryApp.auth().createUserWithEmailAndPassword(em, pwd).then(function(firebaseUser) {
          console.log("User " + firebaseUser.uid + " created successfully!");
          //I don't know if the next statement is necessary
          secondaryApp.auth().signOut();
      }); */
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      displayName: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      role: new FormControl('', Validators.compose([
        Validators.required
      ])),
      brand: new FormControl('', Validators.compose([
        Validators.required
      ])),
    });
  }

  signUp(value) {
    console.log(value);
    //this.ionicAuthService.createUser(value)
    this.ionicAuthService.registerUser(value)
      .then((response) => {
        this.errorMsg = '';
        this.successMsg = 'New user created.';
      }, error => {
        console.log(error.message);
        this.errorMsg = error.message;
        this.successMsg = '';
      });
  }

  goToLogin() {
    this.router.navigateByUrl('login');
  }

  return() {
    this.router.navigateByUrl('roleselector');
  }

}
