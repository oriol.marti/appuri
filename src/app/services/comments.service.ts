import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Comment } from '../interfaces/comment';
import { AuthStorageService } from './auth-storage.service';
import { User } from '../interfaces/users';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(
    private firestore: AngularFirestore,
    private router: Router,
    public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth,) {
  }

  createComment() {
    //se tiene que generar el commentario desde la pagina.
    //const userRef: AngularFirestoreDocument<any> = this.afStore.doc(`comments/${this.commentId}`);
  }

  getComments() {
    return this.firestore.collection('comments').snapshotChanges();
  }

  getCommentsProject(projectid) {
    // eslint-disable-next-line max-len
    return this.firestore.collection('comments', ref => ref.where('projectId', '==', projectid).orderBy('lastEdit', 'asc')).snapshotChanges();
  }

  getComment(id) {
    return this.firestore.collection('comments').doc(id).valueChanges();
  }

  async getAll() {
    // eslint-disable-next-line max-len
    return this.firestore.collection('comments').get().subscribe((response) => { console.log(response.docs.map(doc => doc.data)); return response.docs.map(doc => doc.data); });
  }

  async getFilteredComents(projectid) {
    console.log('projectid= ' + projectid);
    return this.firestore.collection('comments', ref => ref.where('projectId', '==', projectid)).get();
  }

  updateComment(id, comment: Comment) {
    this.firestore.collection('comments').doc(id).update(comment)
      .then(() => {
        //this.router.navigate(['list-Comments']);
      }).catch(error => console.log(error));
  }

  deleteComment(id) {
    this.firestore.doc('comments/' + id).delete();
  }
}
