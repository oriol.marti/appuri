import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  isLoading: boolean;

  private loader: HTMLIonLoadingElement;
  constructor(public loadingController: LoadingController) { }
  async present() {
    this.isLoading = true;
    return await this.loadingController.create({
      // duration: 5000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }


  async showLoader(): Promise<void> {
    this.isLoading = true;
    this.loader = await this.loadingController.create({
      spinner: null,
      message: '<ion-img style="margin-top=-50px;" src="./assets/images/preloader.gif" alt="loading..."></ion-img>',
      keyboardClose: true,
    });

    await this.loader.present();
  }

  async hideLoader(): Promise<void> {
    this.isLoading = false;
    await this.loader.dismiss();
  }
}
