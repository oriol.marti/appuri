import { Injectable } from '@angular/core';
import { LocalNotifications } from '@capacitor/local-notifications';

@Injectable({
  providedIn: 'root'
})
export class LocalNotificationService {

  constructor() { }

  async showLocalNotification(id: number, title: string, text: string) {

    LocalNotifications.schedule({
      notifications: [
        {
          // eslint-disable-next-line object-shorthand
          title: title,
          body: text,
          // eslint-disable-next-line object-shorthand
          id: id
        }
      ]
    });
  }
}
