/* eslint-disable max-len */
import { Injectable, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { User } from '../interfaces/users';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { ActivationStart, Router, RouterOutlet } from '@angular/router';
import { getAuth } from 'firebase/auth';
import { AuthStorageService } from './auth-storage.service';
import { sendPasswordResetEmail } from 'firebase/auth';
import { multiFactor } from 'firebase/auth';
@Injectable({
  providedIn: 'root'
})

export class IonicAuthService {
  [x: string]: any;
  @ViewChild(RouterOutlet) outlet: RouterOutlet;
  userstatus: boolean;
  uservalue: any;
  userData: User = {} as User;
  auth = getAuth();
  originalUser: any;
  auth2: any;

  constructor(
    private angularFireAuth: AngularFireAuth,
    public afStore: AngularFirestore,
    public router: Router,
    private userDataStorage: AuthStorageService,
  ) {

    this.router.events.subscribe(e => {
      if (e instanceof ActivationStart && e.snapshot.outlet === 'login') { this.outlet.deactivate(); }
    });
  }

  get isLoggedIn(): boolean {
    //const user = JSON.parse(atob(localStorage.getItem('user')));
    const user = localStorage.getItem('token');
    return (user !== null || user !== 'null') ? true : false;
  }

  createUser(value: any) {
    return new Promise<any>((resolve, reject) => {
      this.angularFireAuth.createUserWithEmailAndPassword(value.email, value.password)
        .then(
          res => resolve(res),
          err => reject(err));
    });
  }

  async registerUser(value: any) {
    const result = await this.angularFireAuth.createUserWithEmailAndPassword(value.email, value.password);
    const userData: User = {
      email: value.email,
      displayName: value.displayName,
      role: value.role,
      brand: value.brand,
      linkAccount: '',
      assignedProjects: [],
      img: 'https://firebasestorage.googleapis.com/v0/b/connectprojectisladigital.appspot.com/o/filesStorage%2F1664287385879_default-avatar-300x300-1.jpg?alt=media&token=2329d6ea-c266-4bc8-83ad-79667f9b73cc'
    };
    console.log(userData);
    this.setUserData(userData).then(async () => {
      this.angularFireAuth.updateCurrentUser(await this.originalUser).then(() => { this.setLocalData(); }).then(() => {
        this.router.navigate(['/tabs/tab2']);
      });
    });
  }

  setUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afStore.doc(`users/${user.email}`);
    const userData: User = {
      email: user.email,
      displayName: user.displayName,
      role: user.role,
      brand: user.brand,
      linkAccount: user.linkAccount,
      assignedProjects: user.assignedProjects,
      img: user.img,
    };
    return userRef.set(userData);
  }

  setLocalData() {
    this.userDataStorage.getUser(this.auth.currentUser.email).subscribe(
      // eslint-disable-next-line @typescript-eslint/no-shadow
      user => {
        this.userData = {
          email: this.auth.currentUser.email,
          // eslint-disable-next-line @typescript-eslint/dot-notation
          displayName: user['displayName'], //user['displayName']
          // eslint-disable-next-line @typescript-eslint/dot-notation
          role: user['role'],
          // eslint-disable-next-line @typescript-eslint/dot-notation
          brand: user['brand'],
          // eslint-disable-next-line @typescript-eslint/dot-notation
          linkAccount: user['linkAccount'],
          // eslint-disable-next-line @typescript-eslint/dot-notation
          assignedProjects: user['assignedProjects'],
          // eslint-disable-next-line @typescript-eslint/dot-notation
          img: user['img']
        };
        //localStorage.setItem('user', JSON.stringify(this.userData));
        localStorage.setItem('token', btoa(JSON.stringify(this.userData)));
      }
    );
    return true;
  }

  signinUser(value: any) {
    return new Promise<any>((resolve, reject) => {
      this.angularFireAuth.signInWithEmailAndPassword(value.email, value.password)
        .then(
          res => resolve(res),
          err => reject(err)).then(() => { this.setLocalData(); });
    });
  }

  signoutUser() {
    this.router.events.subscribe(e => {
      if (e instanceof ActivationStart && e.snapshot.outlet === 'login') { this.outlet.deactivate(); }
    });
    return new Promise<void>((resolve, reject) => {
      if (this.angularFireAuth.currentUser) {
        this.angularFireAuth.signOut()
          .then(async () => {
            resolve();
            await this.angularFireAuth.signOut().then(() => {
              localStorage.setItem('first', 'false');
              localStorage.setItem('token', 'null');
            });
          }).catch(() => {
            reject();
          });
      }
    });
  }

  currentUser() {
    return this.angularFireAuth.currentUser;
  }

  userDetails() {
    return this.angularFireAuth.user;
  }

  rePassword(email) {
    const auth = getAuth();
    sendPasswordResetEmail(auth, email)
      .then(() => {
        // Password reset email sent!
        // ..
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        // ..
      });
  }
}
