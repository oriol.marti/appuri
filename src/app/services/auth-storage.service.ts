import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { async } from 'rxjs';
import { User } from '../interfaces/users';

@Injectable({
  providedIn: 'root'
})
export class AuthStorageService {

  constructor(private firestore: AngularFirestore, private router: Router,) { }

  createUser() {
    this.router.navigate(['/register']);
  }

  getUsers() {
    return this.firestore.collection('users').snapshotChanges();
  }

  getUser(id) {
    return this.firestore.collection('users').doc(id).valueChanges();
  }

  updateUser(id, user: User) {
    this.firestore.collection('users').doc(id).update(user)
      .then(() => {
        this.router.navigate(['/list-users']);
      }).catch(error => console.log(error));
  }

  deleteUser(id) {
    this.firestore.doc('users/' + id).delete();
  }

  updateProjectsData(id, projectsId) {
    /* this.firestore.collection("users").doc(id)
  .update({"assignedProjects": this.firestore.arrayUnion(projectsId)}); */
    this.firestore.collection('users').doc(id)
      .update({ assignedProjects: projectsId });
  }

  updateLinkAccount(id, linkAccountId) {
    this.firestore.collection('users').doc(id)
      .update({ linkAccount: linkAccountId });
  }

  updateImg(id, urlImg) {
    this.firestore.collection('users').doc(id)
      .update({ img: urlImg });
  }

  editUserDocument(id: string, user: User) {
    this.firestore.collection('users').doc(user.email).set(user);
    //this.firestore.collection('users').doc(id).update(user).then(() => {}).catch(error => console.log(error));
  }

}
