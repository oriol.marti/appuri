/* eslint-disable @typescript-eslint/dot-notation */
/* eslint-disable max-len */
import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Md5 } from 'ts-md5/dist/md5';

@Injectable({
  providedIn: 'root'
})
export class WebServiceService {

  saveErrorData: any;
  project: any;

  constructor(private httpclient: HttpClient,) {

  }

  retrieveQuery(query) {
    return this.httpclient.get<any>('html.//ejemplo' + query);
  }

}
