import { TestBed } from '@angular/core/testing';

import { NetworkstatusService } from './networkstatus.service';

describe('NetworkstatusService', () => {
  let service: NetworkstatusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NetworkstatusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
