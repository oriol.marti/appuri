import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DextailServicesPageRoutingModule } from './dextail-services-routing.module';

import { DextailServicesPage } from './dextail-services.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DextailServicesPageRoutingModule
  ],
  declarations: [DextailServicesPage]
})
export class DextailServicesPageModule {}
