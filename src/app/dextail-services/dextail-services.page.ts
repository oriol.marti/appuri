import { AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import SignaturePad from 'signature_pad';
import { Base64 } from '@ionic-native/base64';
import { MenuController } from '@ionic/angular';
import { LocalNotificationService } from '../services/local-notification.service';
import { LocalNotifications, ScheduleOptions } from '@capacitor/local-notifications';

@Component({
  selector: 'app-dextail-services',
  templateUrl: './dextail-services.page.html',
  styleUrls: ['./dextail-services.page.scss'],
})


export class DextailServicesPage implements OnInit, AfterViewInit, AfterViewInit {
  @ViewChild('canvas', { static: true }) signaturePadElement;
  signaturePad: any;
  canvasWidth: number;
  canvasHeight: number;
  email: string;
  fecha: string;
  date: any;
  //local notifiations
  ids: number[] = [];
  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  resume: boolean = false;

  slideOpts = {
    loop: false
  };

  constructor(
    private elementRef: ElementRef,
    private router: Router,
  ) {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    const yyyy = today.getFullYear();

    this.date = dd+' - '+mm+' - '+yyyy ;
  }

  redirectProjects() {
    this.router.navigateByUrl('/tabs/tab2');
  }

  ngOnInit(): void {
    this.init();
  }


  // eslint-disable-next-line @typescript-eslint/member-ordering
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.init();
  }
  /**
   * Initializing the can.
   *
   * @uri
   */
  init() {
    const canvas: any = this.elementRef.nativeElement.querySelector('canvas');
    canvas.width = window.innerWidth - 35;
    canvas.height = window.innerHeight - 540;
    if (this.signaturePad) {
      this.signaturePad.clear(); // Clear the pad on init
    }
  }

  public ngAfterViewInit(): void {
    this.signaturePad = new SignaturePad(this.signaturePadElement.nativeElement);
    this.signaturePad.clear();
    this.signaturePad.penColor = 'rgb(0,0,0)';
  }

  isCanvasBlank(): boolean {
    if (this.signaturePad) {
      return this.signaturePad.isEmpty() ? true : false;
    }
  }

  clear() {
    this.signaturePad.clear();
  }

  undo() {
    const data = this.signaturePad.toData();
    if (data) {
      data.pop(); // remove the last dot or line
      this.signaturePad.fromData(data);
    }
  }

  save(): void {
    const img = this.signaturePad.toDataURL();
    Base64.encodeFile(img).then(
      res => console.log('Saved image to gallery ', res),
      err => console.log('Error saving image to gallery ', err)
    );

  }
}
