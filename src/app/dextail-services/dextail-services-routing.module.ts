import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DextailServicesPage } from './dextail-services.page';

const routes: Routes = [
  {
    path: '',
    component: DextailServicesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DextailServicesPageRoutingModule {}
