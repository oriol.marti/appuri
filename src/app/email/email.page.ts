/* eslint-disable no-underscore-dangle */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { EmailComposer, EmailComposerOptions } from '@awesome-cordova-plugins/email-composer/ngx';
import { Location } from '@angular/common';
import { Globalization } from '@awesome-cordova-plugins/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-email',
  templateUrl: './email.page.html',
  styleUrls: ['./email.page.scss'],
})
export class EmailPage {
  showOtherTo = false;
  emailForm: FormGroup;
  attachments: any = [];

  formErrors: any = {};

  send: string;
  to: string;
  titl: string;
  body: string;

  public language: string;
  private subscription: any;

  constructor(
    private emailComposer: EmailComposer,
    private formBuilder: FormBuilder,
    // eslint-disable-next-line @typescript-eslint/naming-convention, @typescript-eslint/no-shadow
    public Location: Location,
    private globalization: Globalization,
    private _translate: TranslateService,
    private platform: Platform,

  ) {
    this.emailForm = this.formBuilder.group({
      to: new FormControl({ value: 'info@example.com', disabled: true }, Validators.compose([
        Validators.required,
        Validators.email,
        Validators.maxLength(50)
      ])),
      subject: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
      body: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(500)
      ])),
    });
  }
  /**
   * Return to last page
   *
   * @uri
   */
  lastPage() {
    this.Location.back();
  }
  /**
   * Send email
   *
   * @uri
   */
  sendEmail() {
    // console.log(this.emailForm.value);

    if (!this.emailForm.valid) {
      console.error('Invalid form is not accepted!');
      return false;
    }

    const files = [];



    const email: EmailComposerOptions = {
      ...this.emailForm.value,
      attachments: files,
      isHtml: true
    };

    // Send a text message using default options
    this.emailComposer.open(email);
  }

  /**
   * Threath errors
   *
   * @uri
   */
  getError(input: string) {
    const errors = this.emailForm.controls[input].errors || {};
    const messages = {
      required: `(${input}) field is required`,
      email: `(${input}) field should be a valid email`,
      maxlength: `(${input}) field should be less than `
    };

    for (const error of Object.keys(errors)) {

      if (messages[error]) {
        if (error === 'maxlength' || error === 'minlength') {
          this.formErrors[input] = messages[error] + errors[error].requiredLength + ' chars';
        } else {
          this.formErrors[input] = messages[error];
        }
      } else {
        console.error(`${error} - equivalent message not found!`);
      }

      // console.log(errors);
    }

    if (Object.keys(errors).length === 0) {
      this.formErrors[input] = null;
    }
  }
  ionViewDidEnter() {
        /* LENGUAGE */
        if(localStorage.getItem('lenguage')){
          this.initTranslateDefault(localStorage.getItem('lenguage'));
        }else{
          this.getDeviceLanguage();
        }
  }

  _initialiseTranslation(): void {
    this._translate.get('EMAIL.send').subscribe((res: string) => {
      this.send = res;
    });
    this._translate.get('EMAIL.to').subscribe((res: string) => {
      this.to = res;
    });
    this._translate.get('EMAIL.title').subscribe((res: string) => {
      this.titl = res;
    });
    this._translate.get('EMAIL.body').subscribe((res: string) => {
      this.body = res;
    });
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initTranslate(language: string) {
    if (language) {
      this.language = this._translate.getBrowserLang();
    }
    else {
      // Set your language here
      this.language = 'en';
    }
    this._translateLanguage();;
  }

  initTranslateDefault(language: string) {
    if (language) {
      this.language = language;
    }
    this._translateLanguage();;
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === 'object') {
      this._initTranslate(navigator.language);
    }
    else {
      this.globalization.getPreferredLanguage()
        .then(res => {
          this._initTranslate(res.value);
        })
        .catch(e => {
          // console.log(e);
        });
    }
  }
}

